Color Spectrum
==============

Color picker for iOS. Allow user to pick any color from the visible spectrum.

![alt tag](https://github.com/peturingi/Color-Spectrum/blob/master/github/screenshot.png)
